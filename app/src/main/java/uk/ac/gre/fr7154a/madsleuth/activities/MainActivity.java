package uk.ac.gre.fr7154a.madsleuth.activities;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import uk.ac.gre.fr7154a.madsleuth.R;
import uk.ac.gre.fr7154a.madsleuth.consumer.MSMessage;
import uk.ac.gre.fr7154a.madsleuth.consumer.MSResponse;
import uk.ac.gre.fr7154a.madsleuth.data.MSHelp;
import uk.ac.gre.fr7154a.madsleuth.models.Suspect;

public class MainActivity extends MSBaseActivity {

    private static final String WS_RESPONSE_SUCCESS = "SUCCESS";
    private static final String WS_RESPONSE_FAILURE = "FAILURE";

    private Button goToAddSuspectBtn;
    private Button goToSuspectListAndSearchBtn;
    private Button deleteDataBtn;
    private Button uploadDataBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        goToAddSuspectBtnInit();
        goToSuspectListAndSearchBtnInit();
        deleteDataBtnInit();
        uploadDataBtnInit();

    }

    private void goToAddSuspectBtnInit() {
        goToAddSuspectBtn = (Button) findViewById(R.id.goToAddSuspectBtn);
        goToAddSuspectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addSuspectIntent = new Intent(getApplicationContext(), AddSuspectActivity.class);
                startActivity(addSuspectIntent);
            }
        });
    }

    private void goToSuspectListAndSearchBtnInit() {
        goToSuspectListAndSearchBtn = (Button) findViewById(R.id.goToSuspectListAndSearchBtn);
        goToSuspectListAndSearchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent suspectListAndSearchIntent = new Intent(getApplicationContext(), SuspectListAndSearchActivity.class);
                startActivity(suspectListAndSearchIntent);
            }
        });
    }

    private void deleteDataBtnInit() {
        deleteDataBtn = (Button) findViewById(R.id.deleteDataBtn);
        deleteDataBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Data deletion confirmation")
                        .setMessage("All your data will be erased, are you sure ?")
                        .setNeutralButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                /* Do nothing */
                            }
                        })
                        .setPositiveButton("Yes, I confirm", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                MSHelp.getInstance(getApplicationContext());
                                MSHelp.resetDB();
                            }
                        })
                        .show();
            }
        });
    }

    private void uploadDataBtnInit() {
        uploadDataBtn = (Button) findViewById(R.id.uploadDataBtn);
        uploadDataBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MSHelp.getInstance(getApplicationContext());
                int suspectsCount = MSHelp.SuspectDAO.getSuspectsCount();

                if(suspectsCount > 0) {
                    uploadDataEvent();
                } else {
                    showToast("No data to upload! Please add first at least one suspect!");
                }
            }
        });
    }

    private void uploadDataEvent() {

        new AlertDialog.Builder(this)
                .setTitle(R.string.ma_upload_confirm_dialog_title)
                .setMessage(R.string.ma_upload_confirm_dialog_msg)
                .setNeutralButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        /* Do nothing */
                    }
                })
                .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        proceedEvent();
                    }
                })
                .show();

    }

    private void proceedEvent() {

        if(internetConnOK()) {

            try {

                URL wsURL = new URL(getString(R.string.ws_url));
                HttpURLConnection con = (HttpURLConnection) wsURL.openConnection();

                MSHelp.getInstance(getApplicationContext());
                List<Suspect> suspectList = MSHelp.SuspectDAO.getAllSuspects();

                MSMessage suspectsDataMsg = new MSMessage(suspectList);
                String suspectsDataMsgString = suspectsDataMsg.toJson();

                UploadSuspectsData dataUploadTask = new UploadSuspectsData(con);
                dataUploadTask.execute(suspectsDataMsgString);

            } catch(Exception e) {
                Log.e("FR_ERROR", e.getMessage());
            }


        } else {
            showToast("No active internet connection detected. Please activate your wifi/3g!");
        }

    }

    private boolean internetConnOK() {

        boolean internetConnStatus = false;

        ConnectivityManager connMgr =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        /* returns null if no default network */
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnected()) {
            internetConnStatus = true;
        }

        return internetConnStatus;

    }

    private class UploadSuspectsData extends AsyncTask<String, Integer, String> {

        private static final String SERVER_ERROR = "server_error";
        private HttpURLConnection con;

        public UploadSuspectsData(HttpURLConnection con) {
            this.con = con;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                con.setDoOutput(true);
                con.setRequestMethod("POST");
                con.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");
            } catch (ProtocolException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected String doInBackground(String... params) {

            String res = "";

            try {

                String postParam =
                        getString(R.string.post_field) +
                        "=" +
                        URLEncoder.encode(params[0], "UTF-8");

                con.setFixedLengthStreamingMode(postParam.getBytes().length);
                PrintWriter out = new PrintWriter(con.getOutputStream());
                out.print(postParam);
                out.close();

                int resCode = con.getResponseCode();
                if(resCode == HttpURLConnection.HTTP_OK) {
                    res = readStream(con.getInputStream());
                } else {
                    res = SERVER_ERROR;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return res;
        }

        @Override
        protected void onPostExecute(String responseStr) {
            super.onPostExecute(responseStr);

            if(responseStr.equals(SERVER_ERROR)) {
                showToast(getString(R.string.upload_server_error));
            } else {

                MSResponse msResponse = MSResponse.fromJSON(responseStr);
                String dialogTitle = "", dialogMsg = "";
                TextView customTitle = (TextView) getLayoutInflater().inflate(R.layout.upload_response_dialog_title, null);

                if(msResponse.getUploadResponseCode().equals(WS_RESPONSE_SUCCESS)) {
                    customTitle.setText("DATA UPLOAD SUCCESSFUL");
                    customTitle.setBackgroundColor(getResources().getColor(android.R.color.holo_green_light));
                    dialogMsg =
                            "Your data is now safe." + "\n" +
                            msResponse.getNumber() + " suspects have been uploaded!";
                } else {
                    customTitle.setText("DATA UPLOAD FAILED");
                    customTitle.setBackgroundColor(getResources().getColor(android.R.color.holo_red_light));
                    dialogMsg = "Sorry, the upload failed. Please contact our DevTeam so that the issue can be sorted out. Thank you";
                }

                new AlertDialog.Builder(MainActivity.this)
                        .setCustomTitle(customTitle)
                        .setMessage(dialogMsg)
                        .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                /* Do nothing */
                            }
                        })
                        .show();

            }
        }



        /* Code has been taken from :
        * http://www.mkyong.com/java/how-to-convert-inputstream-to-string-in-java/
        * and has been slightly modified
        * */
        private String readStream(InputStream stream) {

            BufferedReader br = null;
            StringBuilder sb = new StringBuilder();

            String res;
            try {

                br = new BufferedReader(new InputStreamReader(stream));
                while((res = br.readLine()) != null) {
                    sb.append(res);
                }

            } catch (IOException e) {
                Log.e("FR_ERROR", "IO Error: " + e.getMessage());
            } finally {
                if(br != null) {

                    try {
                        br.close();
                    } catch (IOException e) {
                        Log.e("FR_ERROR", "IO Error: " + e.getMessage());
                    }

                }
            }

            return sb.toString();

        }


    }

    private void showToast(String message) {
        Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
    }

}
