package uk.ac.gre.fr7154a.madsleuth.consumer;

import com.google.gson.Gson;

import java.util.List;

import uk.ac.gre.fr7154a.madsleuth.models.Suspect;

/**
 * Created by ddzfred on 10/11/2015.
 */
public class MSMessage {

    private final String userId = "fr7154a";
    private List<Suspect> detailList;

    public MSMessage(List<Suspect> suspectList) {
        this.detailList = suspectList;
    }

    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public String getUserId() {
        return userId;
    }

    public List<Suspect> getDetailList() {
        return detailList;
    }

    public void setDetailList(List<Suspect> detailList) {
        this.detailList = detailList;
    }
}
