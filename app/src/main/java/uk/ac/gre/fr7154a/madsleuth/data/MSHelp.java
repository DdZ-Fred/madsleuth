package uk.ac.gre.fr7154a.madsleuth.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import uk.ac.gre.fr7154a.madsleuth.models.Report;
import uk.ac.gre.fr7154a.madsleuth.models.Suspect;
import uk.ac.gre.fr7154a.madsleuth.tools.MSTools;

/**
 * Created by ddzfred on 05/11/2015.
 */
public class MSHelp extends SQLiteOpenHelper {

    private static MSHelp instance;

    /* Constructor */
    protected MSHelp(Context context) {
        super(context, MSContr.DATABASE_NAME, null, MSContr.DATABASE_VERSION);
    }

    /* Singleton accessor or Init */
    public static MSHelp getInstance(Context context) {
        if (instance == null) {
            instance = new MSHelp(context);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(MSContr.SuspectEntry.CREATE_TABLE);
        db.execSQL(MSContr.SuspectEntry.CREATE_INDEX);
        db.execSQL(MSContr.SuspectEntry.CREATE_TABLE_FTS);
        db.execSQL(MSContr.ReportEntry.CREATE_TABLE);
        db.execSQL(MSContr.ReportEntry.CREATE_INDEX);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            db.execSQL(MSContr.ENABLE_FOREIGN_KEYS);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        /* DROP EVERY TABLE AND INDEX */
        db.execSQL(MSContr.ReportEntry.DROP_INDEX);
        db.execSQL(MSContr.ReportEntry.DROP_TABLE);

        db.execSQL(MSContr.SuspectEntry.DROP_TABLE_FTS);
        db.execSQL(MSContr.SuspectEntry.DROP_INDEX);
        db.execSQL(MSContr.SuspectEntry.DROP_TABLE);


        /* RE-CREATE EVERYTHING */
        onCreate(db);
    }

    public static void resetDB() {
        SQLiteDatabase db = instance.getWritableDatabase();

        db.execSQL(MSContr.ReportEntry.DROP_INDEX);
        db.execSQL(MSContr.ReportEntry.DROP_TABLE);
        db.execSQL(MSContr.SuspectEntry.DROP_TABLE_FTS);
        db.execSQL(MSContr.SuspectEntry.DROP_INDEX);
        db.execSQL(MSContr.SuspectEntry.DROP_TABLE);

        db.execSQL(MSContr.SuspectEntry.CREATE_TABLE);
        db.execSQL(MSContr.SuspectEntry.CREATE_INDEX);
        db.execSQL(MSContr.SuspectEntry.CREATE_TABLE_FTS);
        db.execSQL(MSContr.ReportEntry.CREATE_TABLE);
        db.execSQL(MSContr.ReportEntry.CREATE_INDEX);

        db.close();
    }

    public static abstract class SuspectDAO {

        public static boolean nameIsUnique(long suspectId, String nameDesc) {

            boolean uniqueness = true;

            SQLiteDatabase db = instance.getReadableDatabase();
            Cursor cursor;

            /* 0 when it is called from AddSuspectActivity */
            if (suspectId == 0) {

                cursor = db.rawQuery(
                        "SELECT COUNT(" + MSContr.SuspectEntry._ID + ")" +
                                " FROM " + MSContr.SuspectEntry.TABLE_NAME +
                                " WHERE " + MSContr.SuspectEntry.COL1_NAME_DESC + " = " + "'" + nameDesc + "'",
                        null
                );

                cursor.moveToFirst();
                if (cursor.getInt(0) > 0) {
                    uniqueness = false;
                }

            /* otherwise, the id of the suspect being updated should be specified (SuspectEditActivity) */
            } else {

                cursor = db.query(
                        MSContr.SuspectEntry.TABLE_NAME,
                        MSContr.SuspectEntry.COLUMNS_ID_ONLY,
                        MSContr.SuspectEntry.COL1_NAME_DESC + " = ?",
                        new String[]{nameDesc},
                        null,
                        null,
                        null
                );

                /* if it is another suspect with the same name/desc, then... */
                while (cursor.moveToNext()) {
                    if (cursor.getLong(0) != suspectId) {
                        uniqueness = false;
                    }
                }

            }


            cursor.close();
            db.close();
            return uniqueness;

        }

        public static Suspect addSuspect(String nameDesc, String identityCode, char gender, int height, int weight, String ageRange, String vehicleDesc, String extraInfo) {

            /* Get write access on DB */
            SQLiteDatabase db = instance.getWritableDatabase();

            /* New suspect object initialization */
            Suspect newSuspect = new Suspect(nameDesc, identityCode, gender, height, weight, ageRange, vehicleDesc, extraInfo, new Date());

            /* Content initialization */
            ContentValues newSuspectCtn = new ContentValues();
            newSuspectCtn.put(MSContr.SuspectEntry.COL1_NAME_DESC, newSuspect.getName());
            newSuspectCtn.put(MSContr.SuspectEntry.COL2_IDENTITY_CODE, newSuspect.getIdentityCode());
            newSuspectCtn.put(MSContr.SuspectEntry.COL3_GENDER, String.valueOf(newSuspect.getGender()));
            newSuspectCtn.put(MSContr.SuspectEntry.COL4_HEIGHT, newSuspect.getHeight());
            newSuspectCtn.put(MSContr.SuspectEntry.COL5_WEIGHT, newSuspect.getWeight());
            newSuspectCtn.put(MSContr.SuspectEntry.COL6_AGE_RANGE, newSuspect.getAgeRange());
            newSuspectCtn.put(MSContr.SuspectEntry.COL7_VEHICLE_DESC, newSuspect.getVehicleDesc());
            newSuspectCtn.put(MSContr.SuspectEntry.COL8_EXTRA_INFO, newSuspect.getExtraInfo());
            newSuspectCtn.put(MSContr.SuspectEntry.COL9_CREATION_DATE, MSTools.Date.getDateFormat("").format(newSuspect.getCreationDate()));

            newSuspect.set_id(db.insertOrThrow(MSContr.SuspectEntry.TABLE_NAME, null, newSuspectCtn));

            db.close();

            return newSuspect;

        }

        public static Suspect getSuspectById(long suspectId) throws ParseException {

            Suspect suspect;

            SQLiteDatabase db = instance.getReadableDatabase();

            Cursor cursor = db.query(
                    MSContr.SuspectEntry.TABLE_NAME,
                    MSContr.SuspectEntry.COLUMNS_NO_ID,
                    MSContr.SuspectEntry._ID + " = ?",
                    new String[]{"" + suspectId},
                    null,
                    null,
                    null
            );

            if (cursor.moveToFirst()) {
                suspect = new Suspect(
                        suspectId,
                        cursor.getString(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getInt(3),
                        cursor.getInt(4),
                        cursor.getString(5),
                        cursor.getString(6),
                        cursor.getString(7),
                        cursor.getString(8)
                );
            } else {
                suspect = null;
            }

            cursor.close();
            db.close();
            return suspect;
        }

        public static int updateSuspect(Suspect updatedSuspect) {

            SQLiteDatabase db = instance.getWritableDatabase();

            ContentValues updatedValues = new ContentValues();
            updatedValues.put(MSContr.SuspectEntry.COL1_NAME_DESC, updatedSuspect.getName());
            updatedValues.put(MSContr.SuspectEntry.COL2_IDENTITY_CODE, updatedSuspect.getIdentityCode());
            updatedValues.put(MSContr.SuspectEntry.COL3_GENDER, String.valueOf(updatedSuspect.getGender()));
            updatedValues.put(MSContr.SuspectEntry.COL4_HEIGHT, updatedSuspect.getHeight());
            updatedValues.put(MSContr.SuspectEntry.COL5_WEIGHT, updatedSuspect.getWeight());
            updatedValues.put(MSContr.SuspectEntry.COL6_AGE_RANGE, updatedSuspect.getAgeRange());
            updatedValues.put(MSContr.SuspectEntry.COL7_VEHICLE_DESC, updatedSuspect.getVehicleDesc());
            updatedValues.put(MSContr.SuspectEntry.COL8_EXTRA_INFO, updatedSuspect.getExtraInfo());

            int updateStatus = db.update(
                    MSContr.SuspectEntry.TABLE_NAME,
                    updatedValues,
                    MSContr.SuspectEntry._ID + " = ?",
                    new String[]{"" + updatedSuspect.get_id()});


            db.close();
            return updateStatus;
        }

        public static int deleteSuspect(long suspectId) {

            SQLiteDatabase db = instance.getWritableDatabase();

            int i = db.delete(
                    MSContr.SuspectEntry.TABLE_NAME,
                    MSContr.SuspectEntry._ID + " = ?",
                    new String[]{String.valueOf(suspectId)}
            );

            db.close();
            return i;
        }

        public static ArrayList<Suspect> getAllSuspects() throws ParseException {

            ArrayList<Suspect> suspectList = new ArrayList<>();

            SQLiteDatabase db = instance.getReadableDatabase();
            Cursor suspectCsr = db.query(
                    MSContr.SuspectEntry.TABLE_NAME,
                    MSContr.SuspectEntry.COLUMNS,
                    null,
                    null,
                    null,
                    null,
                    null
            );

            while (suspectCsr.moveToNext()) {

                suspectList.add(new Suspect(
                        suspectCsr.getLong(0),
                        suspectCsr.getString(1),
                        suspectCsr.getString(2),
                        suspectCsr.getString(3),
                        suspectCsr.getInt(4),
                        suspectCsr.getInt(5),
                        suspectCsr.getString(6),
                        suspectCsr.getString(7),
                        suspectCsr.getString(8),
                        suspectCsr.getString(9)
                ));

            }

            suspectCsr.close();
            db.close();

            return suspectList;
        }

        public static int getSuspectsCount() {

            int sCount = 0;

            SQLiteDatabase db = instance.getReadableDatabase();

            Cursor csr = db.rawQuery(
                    "SELECT COUNT(" + MSContr.SuspectEntry._ID + ")" +
                            " FROM " + MSContr.SuspectEntry.TABLE_NAME
                    , null
            );

            if(csr.moveToFirst()) {
                sCount = csr.getInt(0);
            }

            csr.close();
            db.close();
            return sCount;
        }

    }

    public static abstract class ReportDAO {


        public static Report addReport(Date reportDatetime, String reportLocation, String reportNotes, long suspectId) {

            /* Get write access on DB */
            SQLiteDatabase db = instance.getWritableDatabase();

            /* New suspect object initialization */
            Report newReport = new Report(reportDatetime, reportLocation, reportNotes, suspectId, new Date());

            /* Content initialization */
            ContentValues newReportCtn = new ContentValues();
            newReportCtn.put(MSContr.ReportEntry.COL1_REPORT_TIME, MSTools.Date.getDateFormat("").format(newReport.getReportTime()));
            newReportCtn.put(MSContr.ReportEntry.COL2_REPORT_LOCATION, newReport.getReportLocation());
            newReportCtn.put(MSContr.ReportEntry.COL3_REPORT_NOTES, newReport.getReportNotes());
            newReportCtn.put(MSContr.ReportEntry.COL4_CREATION_DATE, MSTools.Date.getDateFormat("").format(newReport.getCreationDate()));
            newReportCtn.put(MSContr.ReportEntry.COL5_SUSPECT_ID_FK, newReport.getSuspectId());

            newReport.set_id(db.insertOrThrow(MSContr.ReportEntry.TABLE_NAME, null, newReportCtn));

            db.close();

            return newReport;

        }

        public static ArrayList<Report> getAllSuspectReports(long suspectId) throws ParseException {

            ArrayList<Report> reportList = new ArrayList<>();

            SQLiteDatabase db = instance.getReadableDatabase();
            Cursor reportCsr = db.query(
                    MSContr.ReportEntry.TABLE_NAME,
                    MSContr.ReportEntry.COLUMNS,
                    MSContr.ReportEntry.COL5_SUSPECT_ID_FK + " = ?",
                    new String[]{"" + suspectId},
                    null,
                    null,
                    null
            );

            while (reportCsr.moveToNext()) {

                reportList.add(new Report(
                        reportCsr.getLong(0),
                        reportCsr.getString(1),
                        reportCsr.getString(2),
                        reportCsr.getString(3),
                        reportCsr.getString(4),
                        reportCsr.getLong(5)
                ));

            }

            reportCsr.close();
            db.close();

            return reportList;
        }

    }

}
