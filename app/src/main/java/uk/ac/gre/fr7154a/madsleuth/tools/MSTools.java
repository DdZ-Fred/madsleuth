package uk.ac.gre.fr7154a.madsleuth.tools;

import android.content.Context;
import java.text.SimpleDateFormat;
import uk.ac.gre.fr7154a.madsleuth.R;

/**
 * Created by ddzfred on 01/11/2015.
 * GATHERS REUSABLE COMPONENTS/FUNCTIONS...
 */
public class MSTools {

    public static class Extra {

        public static final String CHILD_ACTIVITY_RESULT = "result";
        public static final String SUSPECT_ACTIVITY_EXTRA_NEW = "newSuspect";
        public static final String SUSPECT_ACTIVITY_EXTRA_EXISTENT = "suspectId";
        public static final String SUSPECT_EDIT_ACTIVITY_EXTRA = "currentSuspect";
//        public static final String SUSPECT_EDIT_REPORTS_ACTIVITY_EXTRA = "suspectId";
    }

    public static class Text {

        public static String textCheck(Context ctx, String textViewTxt) {
            return textViewTxt.isEmpty() ?
                    ctx.getResources().getText(R.string.no_data_default_txt).toString()
                    :
                    textViewTxt;
        }

    }

    /* Gathers all Date tools */
    public static class Date {

        public static final String APP_DATE_FRMT = "dd/MM/yyyy";
        public static final String APP_TIME_FRMT = "h:mm a"; // Hour:Minutes (AM or PM)
        public static final String APP_DATETIME_FRMT = "dd/MM/yyyy h:mm a";
        public static final String APP_REPORT_TIME_FRMT = "HH:mm - dd/MM/yyyy";

        /* creationDate and reportTime formatted this way in the DB */
        public static final String DEFAULT_DB_DATETIME_FRMT = "dd/MM/yyyy HH:mm:ss:SSS";

        public static SimpleDateFormat getDateFormat(String type){
            switch(type) {
                case "ad":
                    return new SimpleDateFormat(APP_DATE_FRMT);
                case "at":
                    return new SimpleDateFormat(APP_TIME_FRMT);
                case "adt":
                    return new SimpleDateFormat(APP_DATETIME_FRMT);
                case "dddt":
                    return new SimpleDateFormat(DEFAULT_DB_DATETIME_FRMT);
                case "art":
                    return new SimpleDateFormat(APP_REPORT_TIME_FRMT);

                default:
                    return new SimpleDateFormat(DEFAULT_DB_DATETIME_FRMT);
            }
        }
    }


    public static class Gender {

        public static char getGenderChar(String genderStr) {
            switch (genderStr) {
                case "Male":
                    return 'm';
                case "Female":
                    return 'f';
                case "Unknown":
                    return 'u';
                default:
                    return 'u';
            }
        }

        public static String getGenderStr(char genderChar) {
            switch (genderChar) {
                case 'm':
                    return "Male";
                case 'f':
                    return "Female";
                case 'u':
                    return "Unknown";
                default:
                    return "unknown";
            }
        }

    }

}
