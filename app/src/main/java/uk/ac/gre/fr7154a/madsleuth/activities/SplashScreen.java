package uk.ac.gre.fr7154a.madsleuth.activities;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.LinearLayout;

import uk.ac.gre.fr7154a.madsleuth.R;


public class SplashScreen extends AppCompatActivity {


    private LinearLayout ssContainer;

    private AnimatorSet mainAnimSet;
    private ObjectAnimator fadeIn;
    private ObjectAnimator blinking;

    private ObjectAnimator scaleX;
    private ObjectAnimator scaleY;
    private AnimatorSet scaleDisappearance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        ssContainer = (LinearLayout) findViewById(R.id.ssContainer);

        fadeIn = ObjectAnimator.ofFloat(ssContainer, "alpha", 0.0f, 1.0f);
        fadeIn.setDuration(1500); //1500

        blinking = ObjectAnimator.ofFloat(ssContainer,"alpha", 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f);
        blinking.setDuration(300);//300

        scaleX = ObjectAnimator.ofFloat(ssContainer, "scaleX", 1.0f, 0.0f);
        scaleY = ObjectAnimator.ofFloat(ssContainer, "scaleY", 1.0f, 0.0f);
        scaleDisappearance = new AnimatorSet();
        scaleDisappearance.setDuration(300);//300
        scaleDisappearance.playTogether(scaleX, scaleY);

        mainAnimSet = new AnimatorSet();
        mainAnimSet.playSequentially(fadeIn, blinking, scaleDisappearance);
        scaleDisappearance.setInterpolator(new AccelerateDecelerateInterpolator());
        scaleDisappearance.setStartDelay(200);//200
        scaleDisappearance.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        scaleDisappearance.start();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("FR_DEBUG", "SPLASH SCREEN - OnStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("FR_DEBUG", "SPLASH SCREEN - OnStop");

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("FR_DEBUG", "SPLASH SCREEN - OnDestroy");
    }
}
