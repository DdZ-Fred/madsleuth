package uk.ac.gre.fr7154a.madsleuth.activities;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import uk.ac.gre.fr7154a.madsleuth.R;
import uk.ac.gre.fr7154a.madsleuth.adapters.MSCursorAdapter;
import uk.ac.gre.fr7154a.madsleuth.data.MSContr;
import uk.ac.gre.fr7154a.madsleuth.data.MSHelp;
import uk.ac.gre.fr7154a.madsleuth.tools.MSTools;

public class SuspectListAndSearchActivity extends MSBaseActivity {

    private SQLiteDatabase db;
    private MSCursorAdapter slsCursorAdapt;
    private ListView slsSuspectListLV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suspect_list_and_search);

        listViewInit();

    }

    @Override
    protected void onStart() {
        Log.d("FR_DEBUG", this.getClass().getSimpleName() + " - OnStart");

        db = MSHelp.getInstance(getApplicationContext()).getReadableDatabase();
        LoadSuspectDetails loadDetails = new LoadSuspectDetails();
        loadDetails.execute("");

        super.onStart();
    }

    private void listViewInit() {

        slsSuspectListLV = (ListView) findViewById(R.id.slsSuspectList);
        slsCursorAdapt = new MSCursorAdapter(this, null, 0);
        slsSuspectListLV.setAdapter(slsCursorAdapt);
        slsSuspectListLV.setEmptyView(findViewById(R.id.slsNoDataView));
        slsSuspectListLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView hiddenSuspectIdTV = (TextView) view.findViewById(R.id.listViewItem_suspectId);
                String suspectId = hiddenSuspectIdTV.getText().toString();

                supportInvalidateOptionsMenu();

                Intent clickedSuspectDetailsIntent = new Intent(SuspectListAndSearchActivity.this, SuspectActivity.class);
                clickedSuspectDetailsIntent.putExtra(MSTools.Extra.SUSPECT_ACTIVITY_EXTRA_EXISTENT, Long.parseLong(suspectId));
                startActivity(clickedSuspectDetailsIntent);
            }
        });

    }

    private class LoadSuspectDetails extends AsyncTask<String, Integer, Cursor> {

        @Override
        protected Cursor doInBackground(String... params) {

            Cursor results;

            /* Default search (called when the activity starts) */
            if (params[0].equals("")) {
                results = db.query(
                        MSContr.SuspectEntry.TABLE_NAME_FTS,
                        MSContr.SuspectEntry.FTS_COLUMNS_ID_AND_NAME,
                        null,
                        null,
                        null,
                        null,
                        null

                );
            } else {

                /* Appending the '*' to the user string allows to search
                 * every record that STARTS with the given string */
                results = db.query(
                        MSContr.SuspectEntry.TABLE_NAME_FTS,
                        MSContr.SuspectEntry.FTS_COLUMNS_ID_AND_NAME,
                        MSContr.SuspectEntry.COL1_NAME_DESC + " MATCH ?",
                        new String[]{params[0] + "*"},
                        null,
                        null,
                        null

                );
            }

            return results;
        }

        @Override
        protected void onPostExecute(Cursor csr) {
            slsCursorAdapt.changeCursor(csr);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(
                R.menu.activity_suspect_search_and_list_toolbar_menu,
                menu);

        MenuItem searchItem = menu.findItem(R.id.slsMenuSearchBtn);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                LoadSuspectDetails newSearch = new LoadSuspectDetails();
                newSearch.execute(newText);

                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onStop() {
        Log.d("FR_DEBUG", this.getClass().getSimpleName() + " - OnStop");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d("FR_DEBUG", this.getClass().getSimpleName() + " - OnDestroy");
        super.onDestroy();
    }

}
