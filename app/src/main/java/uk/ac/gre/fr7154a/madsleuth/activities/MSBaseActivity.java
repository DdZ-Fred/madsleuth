package uk.ac.gre.fr7154a.madsleuth.activities;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import uk.ac.gre.fr7154a.madsleuth.R;

public abstract class MSBaseActivity extends AppCompatActivity {

    private DrawerLayout msBaseDrawer;
    private NavigationView msBaseNav;
    private ActionBarDrawerToggle msBaseDrawerToggle;
    private FrameLayout msBaseContent;
    private Toolbar msBaseToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        msBaseDrawer = (DrawerLayout) getLayoutInflater().inflate(R.layout.activity_ms_base, null);
        msBaseNav = (NavigationView) msBaseDrawer.findViewById(R.id.msBaseNav);
    }

    @Override
    public void setContentView(int layoutResID) {

        /* Stores the main content */
        msBaseContent = (FrameLayout) msBaseDrawer.findViewById(R.id.msBaseContent);
        getLayoutInflater().inflate(layoutResID, msBaseContent, true);

        super.setContentView(msBaseDrawer);

        /* Toolbar/ActionBar Init */
        msBaseToolbar = (Toolbar) findViewById(R.id.msBaseToolbar);
        setSupportActionBar(msBaseToolbar);

        msBaseDrawerInit();

    }

    private void msBaseDrawerInit() {

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_reorder_black_24dp);


        msBaseDrawerToggle = new ActionBarDrawerToggle(
                this,
                msBaseDrawer,
                msBaseToolbar,
                R.string.drawer_open,
                R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getSupportActionBar().setTitle(getTitle());
                supportInvalidateOptionsMenu();

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle(R.string.drawer_title);
                supportInvalidateOptionsMenu();

            }
        };
        msBaseDrawer.setDrawerListener(msBaseDrawerToggle);


        /* Drawer Menu Listener */
        msBaseNav.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(MenuItem item) {

                switch (item.getItemId()) {

                    case R.id.msBaseDrawerMenuHome:
                        msBaseDrawer.closeDrawer(msBaseNav);

                        if (!getTitle().equals(getResources().getString(R.string.main_activity_label))) {
                            Intent mainIntent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(mainIntent);
                        }

                        return true;

                    case R.id.msBaseDrawerMenuAddSuspect:
                        msBaseDrawer.closeDrawer(msBaseNav);

                        if (!getTitle().equals(getResources().getString(R.string.add_suspect_activity_label))) {
                            Intent addSuspectIntent = new Intent(getApplicationContext(), AddSuspectActivity.class);
                            startActivity(addSuspectIntent);
                        }

                        return true;

                    case R.id.msBaseDrawerMenuSuspectList:
                        msBaseDrawer.closeDrawer(msBaseNav);

                        if (!getTitle().equals(getResources().getString(R.string.suspect_list_and_search_activity_label))) {
                            Intent suspectListAndSearchIntent = new Intent(getApplicationContext(), SuspectListAndSearchActivity.class);
                            startActivity(suspectListAndSearchIntent);
                        }

                        return true;

                    default:
                        return true;

                }
            }
        });
    }



    /* Called whenever invalidateOptionsMenu is called.
    * Useful when hiding stuff is necessary*/
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {


        return super.onPrepareOptionsMenu(menu);
    }

    private void showToast(String message) {
        Toast.makeText(MSBaseActivity.this, message, Toast.LENGTH_SHORT).show();
    }

}
