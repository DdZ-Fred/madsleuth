package uk.ac.gre.fr7154a.madsleuth.adapters;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import uk.ac.gre.fr7154a.madsleuth.R;
import uk.ac.gre.fr7154a.madsleuth.models.Report;
import uk.ac.gre.fr7154a.madsleuth.tools.MSTools;

/**
 * Created by ddzfred on 11/12/2015.
 */
public class MSReportListAdapter extends BaseAdapter {

    private Context ctx;
    private int res;
    private List<Report> reports;
    private LayoutInflater li;
    private SimpleDateFormat df;



    public MSReportListAdapter(Context context, int resource, List<Report> reports) {
        this.ctx = context;
        this.res = resource;
        this.reports = reports;
        li = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        df = MSTools.Date.getDateFormat("art");
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = li.inflate(res, parent, false);

        TextView reportIdx = (TextView) convertView.findViewById(R.id.saReportLvItemIdx);
        TextView reportTime = (TextView) convertView.findViewById(R.id.saReportLvItemTime);
        TextView reportLocation = (TextView) convertView.findViewById(R.id.saReportLvItemLocation);
        TextView reportNotes = (TextView) convertView.findViewById(R.id.saReportLvItemNotes);

        Report report = (Report) getItem(position);

        reportIdx.setText("#" + (position + 1));
        reportTime.setText(df.format(report.getReportTime()));
        reportLocation.setText(report.getReportLocation());
        reportNotes.setText(report.getReportNotes());


        return convertView;
    }

    @Override
    public long getItemId(int position) {
        return ((Report) getItem(position)).get_id();
    }

    @Override
    public Object getItem(int position) {
        return this.reports.get(position);
    }

    @Override
    public int getCount() {
        return this.reports.size();
    }
}
