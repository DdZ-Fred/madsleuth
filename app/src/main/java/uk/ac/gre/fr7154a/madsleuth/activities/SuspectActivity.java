package uk.ac.gre.fr7154a.madsleuth.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import uk.ac.gre.fr7154a.madsleuth.R;
import uk.ac.gre.fr7154a.madsleuth.adapters.MSReportListAdapter;
import uk.ac.gre.fr7154a.madsleuth.data.MSContr;
import uk.ac.gre.fr7154a.madsleuth.data.MSHelp;
import uk.ac.gre.fr7154a.madsleuth.models.Report;
import uk.ac.gre.fr7154a.madsleuth.models.Suspect;
import uk.ac.gre.fr7154a.madsleuth.tools.MSTools;

public class SuspectActivity extends MSBaseActivity {

    private static final int SEA_REQ_CODE = 1;

    private Suspect currentSuspect;

    private TextView saTitle;
    private TextView saSuspectNameDesc;
    private TextView saSuspectIdentityCode;
    private TextView saSuspectGender;
    private TextView saSuspectHeight;
    private TextView saSuspectWeight;
    private TextView saSuspectAgeRange;
    private TextView saSuspectVehicleDesc;
    private TextView saSuspectExtraInfo;

    private Button saEditSuspectBtn;
    private Button saAddSuspectReportBtn;
    private Button saDeleteSuspectBtn;

    private ListView saReportLV;
    private MSReportListAdapter saReportLA;
    private List<Report> saReports;
    private TextView saReportEmptyView;

    private LayoutInflater li;
    private Calendar cal;
    private View newSuspectReportView;
    private TimePicker reportTimePicker;
    private EditText reportLocationInput;
    private EditText reportNotesInput;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("FR_DEBUG", this.getClass().getSimpleName() + " - OnCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suspect);

        /* CHECK INTENT EXTRA CONTENT */
        if (getIntent().getParcelableExtra(MSTools.Extra.SUSPECT_ACTIVITY_EXTRA_NEW) != null) {
            currentSuspect = getIntent().getParcelableExtra(MSTools.Extra.SUSPECT_ACTIVITY_EXTRA_NEW);
        } else {

            /* Get Suspect id in Intent extra and find Suspect in DB */
            long currentSuspectId = getIntent().getLongExtra(MSTools.Extra.SUSPECT_ACTIVITY_EXTRA_EXISTENT, 0);
            MSHelp.getInstance(getApplicationContext());
            try {
                currentSuspect = MSHelp.SuspectDAO.getSuspectById(currentSuspectId);
            } catch (ParseException pe) {
                Log.e("PARSING ERROR", "Retrieving suspect failed: " + pe.getMessage());
            }

        }

        layoutInit();

    }

    private void layoutInit() {

        saTitle = (TextView) findViewById(R.id.saTitle);
        saSuspectNameDesc = (TextView) findViewById(R.id.saSuspectNameDesc);
        saSuspectIdentityCode = (TextView) findViewById(R.id.saSuspectIdentityCode);
        saSuspectGender = (TextView) findViewById(R.id.saSuspectGender);
        saSuspectHeight = (TextView) findViewById(R.id.saSuspectHeight);
        saSuspectWeight = (TextView) findViewById(R.id.saSuspectWeight);
        saSuspectAgeRange = (TextView) findViewById(R.id.saSuspectAgeRange);
        saSuspectVehicleDesc = (TextView) findViewById(R.id.saSuspectVehicleDesc);
        saSuspectExtraInfo = (TextView) findViewById(R.id.saSuspectExtraInfo);

        cal = Calendar.getInstance();
        li = getLayoutInflater();

        reportsListViewInit();
        feedViews();
        buttonsInit();

    }

    private void reportsListViewInit() {

        /* ListView component init */
        saReportLV = (ListView) findViewById(R.id.saReportsLV);

        /* Listview data init */
        MSHelp.getInstance(getApplicationContext());
        saReports = new ArrayList<>();
        try {
            saReports = MSHelp.ReportDAO.getAllSuspectReports(currentSuspect.get_id());
        } catch (ParseException pe) {
            System.err.println("Error gathering the suspect Reports: " + pe.getMessage());
        }


        /* Listview adapter init */
        saReportLA = new MSReportListAdapter(
                getApplicationContext(),
                R.layout.activity_suspect_report_listview_item,
                saReports
        );
        saReportLV.setAdapter(saReportLA);
        saReportLV.setEmptyView(findViewById(R.id.saReportsLvEmptyView));
    }


    /* Called each time the 'Add report' button is clicked */
    private void newSuspectReportDialogInit() {

        newSuspectReportView = li.inflate(R.layout.dialog_suspect_add_report, null);
        reportTimePicker = (TimePicker) newSuspectReportView.findViewById(R.id.nsrTimeInput);
        reportLocationInput = (EditText) newSuspectReportView.findViewById(R.id.nsrLocationInput);
        reportNotesInput = (EditText) newSuspectReportView.findViewById(R.id.nsrNotesInput);
    }

    private void feedViews() {

        saTitle.setText(getResources().getString(R.string.suspect_activity_title_txt) + String.valueOf(currentSuspect.get_id()));
        saSuspectNameDesc.setText(currentSuspect.getName());
        saSuspectIdentityCode.setText(currentSuspect.getIdentityCode());
        saSuspectGender.setText(MSTools.Gender.getGenderStr(currentSuspect.getGender()));
        saSuspectHeight.setText(currentSuspect.getHeight() + "cm");
        saSuspectWeight.setText(currentSuspect.getWeight() + "kg");
        saSuspectAgeRange.setText(currentSuspect.getAgeRange());
        saSuspectVehicleDesc.setText(MSTools.Text.textCheck(getApplicationContext(), currentSuspect.getVehicleDesc()));
        saSuspectExtraInfo.setText(MSTools.Text.textCheck(getApplicationContext(), currentSuspect.getExtraInfo()));

    }


    private void buttonsInit() {

        saEditSuspectBtn = (Button) findViewById(R.id.saEditSuspectBtn);
        saAddSuspectReportBtn = (Button) findViewById(R.id.saEditSuspectReportsBtn);
        saDeleteSuspectBtn = (Button) findViewById(R.id.saDeleteSuspectBtn);



        saEditSuspectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /* sea = Suspect Edit Activity */
                Intent seaIntent = new Intent(getApplicationContext(), SuspectEditActivity.class);
                seaIntent.putExtra(MSTools.Extra.SUSPECT_EDIT_ACTIVITY_EXTRA, currentSuspect);
                startActivityForResult(seaIntent, SEA_REQ_CODE);
            }
        });


        saAddSuspectReportBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addSuspectReportBtnEvent();
            }
        });

        saDeleteSuspectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(SuspectActivity.this)
                        .setTitle(R.string.suspect_del_dialog_title)
                        .setMessage("Suspect n" + currentSuspect.get_id() + " will be deleted, please confirm:")
                        .setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                /* Do nothing */
                            }
                        })
                        .setPositiveButton("Yes, delete it", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                MSHelp.getInstance(getApplicationContext());
                                int deleteStatus = MSHelp.SuspectDAO.deleteSuspect(currentSuspect.get_id());

                                if (deleteStatus == 1) {
                                    DeleteSuspectFTS asyncDeleteFTS = new DeleteSuspectFTS();
                                    asyncDeleteFTS.execute(currentSuspect.get_id());
                                }

                                Intent mainActivityIntent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(mainActivityIntent);
                            }
                        })
                        .show();
            }
        });

    }

    private void addSuspectReportBtnEvent() {

        newSuspectReportDialogInit();

        reportTimePicker.setIs24HourView(true);

                /* Note: setCurrentHour and setCurrentMinute are deprecated starting from
                 * API ver 23, which is my target SDK version. However my min SDK version is 21,
                 * it is why i'm using them. */
        cal.setTime(new Date());
        reportTimePicker.setCurrentHour(cal.get(Calendar.HOUR_OF_DAY));
        reportTimePicker.setCurrentMinute(cal.get(Calendar.MINUTE));


        new AlertDialog.Builder(SuspectActivity.this)
                .setTitle("Enter new report info:")
                .setView(newSuspectReportView)
                .setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                                /* Do nothing */
                    }
                })
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                                /* Get time */
                        int reportHour = reportTimePicker.getCurrentHour();
                        int reportMinute = reportTimePicker.getCurrentMinute();


                                /* Default is current time but the user is able to change so... */
                        Date reportDatetime;
                        cal.set(Calendar.HOUR_OF_DAY, reportHour);
                        cal.set(Calendar.MINUTE, reportMinute);
                        reportDatetime = cal.getTime();

                                /* Get location */
                        String reportLocation = reportLocationInput.getText().toString();

                                /* Get notes */
                        String reportNotes = reportNotesInput.getText().toString();

                        MSHelp.getInstance(getApplicationContext());
                        Report newReport = MSHelp.ReportDAO.addReport(
                                reportDatetime,
                                reportLocation,
                                reportNotes,
                                currentSuspect.get_id()
                        );
                        saReports.add(newReport);
                        saReportLA.notifyDataSetChanged();

                        Toast.makeText(SuspectActivity.this,
                                "Report list updated",
                                Toast.LENGTH_SHORT).show();


                    }
                }).show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {

            case SEA_REQ_CODE:

                switch (resultCode) {

                    case RESULT_OK:

                        currentSuspect = data.getParcelableExtra(MSTools.Extra.CHILD_ACTIVITY_RESULT);
                        feedViews();

                        break;
                    case RESULT_CANCELED:

                        /* Data check */
                        switch (data.getStringExtra(MSTools.Extra.CHILD_ACTIVITY_RESULT)) {

                            case "updateFailed":
                                showToast("Error during suspect update. Please try again!");
                                break;

                            case "noData":
                                showToast("Error: Couldn't get suspect details!");
                                break;

                            case "editCancelled":
                                showToast("Modifications not saved!");
                                break;

                            default:
                                break;
                        }

                        break;
                    default:
                        break;
                }


                break;
            case 2:
                break;
            default:
                break;

        }

    }

    /* Long param = suspectId */
    private class GetAllSuspectReports extends AsyncTask<Long, Integer, List<Report>> {
        @Override
        protected List<Report> doInBackground(Long... params) {

            List<Report> allSuspectReports = null;

            MSHelp.getInstance(getApplicationContext());
            try {
                allSuspectReports = MSHelp.ReportDAO.getAllSuspectReports(params[0]);
            } catch (ParseException e) {
                e.printStackTrace();
            }


            return allSuspectReports;
        }

        @Override
        protected void onPostExecute(List<Report> reports) {
            super.onPostExecute(reports);
            /* Use list here to update the ListView */
        }
    }


    private class DeleteSuspectFTS extends AsyncTask<Long, Integer, Integer> {

        @Override
        protected Integer doInBackground(Long... params) {

            SQLiteDatabase db = MSHelp.getInstance(getApplicationContext()).getWritableDatabase();

            int deleteStatus = db.delete(
                    MSContr.SuspectEntry.TABLE_NAME_FTS,
                    MSContr.SuspectEntry.COL_DOCID + " = ?",
                    new String[] {String.valueOf(params[0])}
            );

            db.close();
            return deleteStatus;
        }

        @Override
        protected void onPostExecute(Integer deleteStatus) {
            super.onPostExecute(deleteStatus);
            Log.d("FR_DEBUG", "deleteSuspectFTS status: " + deleteStatus);
        }
    }


    private void showToast(String message) {
        Toast.makeText(SuspectActivity.this, message, Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onResume() {
        Log.d("FR_DEBUG", this.getClass().getSimpleName() + " - OnResume");
        super.onResume();
    }

    @Override
    protected void onStart() {
        Log.d("FR_DEBUG", this.getClass().getSimpleName() + " - OnStart");
        super.onStart();
    }

    @Override
    protected void onStop() {
        Log.d("FR_DEBUG", this.getClass().getSimpleName() + " - OnStop");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d("FR_DEBUG", this.getClass().getSimpleName() + " - OnDestroy");
        super.onDestroy();
    }

}
