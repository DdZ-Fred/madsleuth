package uk.ac.gre.fr7154a.madsleuth.consumer;

import com.google.gson.Gson;

/**
 * Created by ddzfred on 10/11/2015.
 */
public class MSResponse {

    private String uploadResponseCode;
    private String userid;
    private String number;
    private String names;
    private String message;

    public MSResponse(){}

    public MSResponse(String upResCode,
                      String userid,
                      String number,
                      String names,
                      String message) {
        this.uploadResponseCode = upResCode;
        this.userid = userid;
        this.number = number;
        this.names = names;
        this.message = message;
    }


    public static MSResponse fromJSON(String jsonResponse) {
        Gson gson = new Gson();
        return gson.fromJson(jsonResponse, MSResponse.class);
    }

    public String getUploadResponseCode() {
        return uploadResponseCode;
    }

    public void setUploadResponseCode(String uploadResponseCode) {
        this.uploadResponseCode = uploadResponseCode;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
