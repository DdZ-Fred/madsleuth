package uk.ac.gre.fr7154a.madsleuth.interfaces;

/**
 * Created by ddzfred on 07/11/2015.
 */
public interface HeightAndWeightAble {

    int HEIGHT_INTERVAL = 5;
    int HEIGHT_MIN = 50;
    int HEIGHT_MAX = 240;

    int WEIGHT_INTERVAL = 10;
    int WEIGHT_MIN = 30;
    int WEIGHT_MAX = 250;

    void heightAndWeightInputsInit();
    int getRealValue(char type, int pickerValue);

}
