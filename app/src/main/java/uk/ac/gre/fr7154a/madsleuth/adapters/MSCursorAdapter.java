package uk.ac.gre.fr7154a.madsleuth.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import uk.ac.gre.fr7154a.madsleuth.R;

/**
 * Created by ddzfred on 10/11/2015.
 */
public class MSCursorAdapter extends CursorAdapter {

    public MSCursorAdapter(Context ctx, Cursor csr, int flags){
        super(ctx, csr, flags);
    }

    @Override
    public View newView(Context ctx, Cursor csr, ViewGroup parent) {
        return LayoutInflater.from(ctx).inflate(
                R.layout.activity_suspect_list_and_search_listview_item,
                parent,
                false
        );
    }

    @Override
    public void bindView(View v, Context ctx, Cursor csr) {

        TextView itemSuspectId = (TextView) v.findViewById(R.id.listViewItem_suspectId);
        TextView itemSuspectNameDesc = (TextView) v.findViewById(R.id.listViewItem_suspectNameDesc);

        /* Cursor Index 0 : _id
         * Cursor Index 1 : suspectNameDesc
         * */
        itemSuspectId.setText(csr.getString(0));
        itemSuspectNameDesc.setText(csr.getString(1));

    }

}
