package uk.ac.gre.fr7154a.madsleuth.activities;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;


import uk.ac.gre.fr7154a.madsleuth.R;
import uk.ac.gre.fr7154a.madsleuth.data.MSContr;
import uk.ac.gre.fr7154a.madsleuth.data.MSHelp;
import uk.ac.gre.fr7154a.madsleuth.interfaces.HeightAndWeightAble;
import uk.ac.gre.fr7154a.madsleuth.models.Suspect;
import uk.ac.gre.fr7154a.madsleuth.tools.MSTools;

public class AddSuspectActivity extends MSBaseActivity implements HeightAndWeightAble {

//    private static final int HEIGHT_INTERVAL = 5;
//    private static final int HEIGHT_MIN = 50;
//    private static final int HEIGHT_MAX = 240;
//
//    private static final int WEIGHT_INTERVAL = 10;
//    private static final int WEIGHT_MIN = 30;
//    private static final int WEIGHT_MAX = 250;


    private EditText suspectNameDescInput;

    private Spinner suspectIdentityCodeInput;
    private ArrayAdapter<CharSequence> icCodeSpAdapter;

    private RadioGroup suspectGenderRG;
    private RadioButton checkedGenderRB;

    private NumberPicker suspectHeightInput;
    private int heightValuesLength;
    private String[] heightValues;

    private NumberPicker suspectWeightInput;
    private int weightValuesLength;
    private String[] weightValues;

    private Spinner suspectAgeRangeInput;
    private ArrayAdapter<CharSequence> ageSpAdapter;

    private EditText suspectVehicleDescInput;

    private EditText suspectExtraInfoInput;

    private Button addSuspectBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_suspect);

        heightAndWeightInputsInit();

        /* Age Spinner Init */
        suspectAgeRangeInput = (Spinner) findViewById(R.id.suspectAgeRangeInput);
        ageSpAdapter = ArrayAdapter.createFromResource(this,R.array.age_ranges, android.R.layout.simple_spinner_item);
        ageSpAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        suspectAgeRangeInput.setAdapter(ageSpAdapter);

        /* IdentityCode Spinner Init */
        suspectIdentityCodeInput = (Spinner) findViewById(R.id.suspectIdentityCodeInput);
        icCodeSpAdapter = ArrayAdapter.createFromResource(this, R.array.identity_codes, android.R.layout.simple_spinner_item);
        icCodeSpAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        suspectIdentityCodeInput.setAdapter(icCodeSpAdapter);


        addSuspectBtn = (Button) findViewById(R.id.addSuspectBtn);
        addSuspectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addSuspectBtnEvent();
            }
        });
    }

    @Override
    public void heightAndWeightInputsInit() {

        /* Suspect Height Input Init */
        suspectHeightInput = (NumberPicker) findViewById(R.id.suspectHeightInput);
        heightValuesLength = (HEIGHT_MAX - HEIGHT_MIN) / HEIGHT_INTERVAL + 1;
        heightValues = new String[heightValuesLength];
        for(int i=0; i < heightValuesLength; i++) {
            heightValues[i] = Integer.toString(HEIGHT_MIN + i * HEIGHT_INTERVAL);
        }
        /* Because of the use of a custom interval, the max value cannot assigned the actual maxValue wanted.
        * The default behaviour is an interval of 1, therefore if the actual maxValue (240) wanted is set,
        * the Picker will behave like there are 240 possible values and this is not he case */
        suspectHeightInput.setMinValue(0);
        suspectHeightInput.setMaxValue(heightValuesLength - 1);
        suspectHeightInput.setWrapSelectorWheel(false);
        suspectHeightInput.setDisplayedValues(heightValues);


        /* Suspect Weight Input Init*/
        suspectWeightInput = (NumberPicker) findViewById(R.id.suspectWeightInput);
        weightValuesLength = (WEIGHT_MAX - WEIGHT_MIN) / WEIGHT_INTERVAL + 1;
        weightValues = new String[weightValuesLength];
        for(int j=0; j < weightValuesLength; j++) {
            weightValues[j] = Integer.toString(WEIGHT_MIN + (j*WEIGHT_INTERVAL));
        }
        suspectWeightInput.setMinValue(0);
        suspectWeightInput.setMaxValue(weightValuesLength - 1);
        suspectWeightInput.setWrapSelectorWheel(false);
        suspectWeightInput.setDisplayedValues(weightValues);
    }

    @Override
    public int getRealValue(char type, int pickerValue) {

        switch(type) {

            /*HEIGHT*/
            case 'h':
                return HEIGHT_MIN + pickerValue * HEIGHT_INTERVAL;

            /*WEIGHT*/
            case 'w':
                return WEIGHT_MIN + pickerValue * WEIGHT_INTERVAL;

            default:
                Log.d("FR_DEBUG", "Wrong type");
                return 0;
        }
    }

     private void addSuspectBtnEvent() {

        /* Required fields */
        suspectNameDescInput = (EditText) findViewById(R.id.suspectNameDescInput);
        suspectGenderRG = (RadioGroup) findViewById(R.id.suspectGenderRadioGroup);

        if(suspectNameDescInput.getText().toString().isEmpty() ||
                suspectGenderRG.getCheckedRadioButtonId() == -1) {
            showToast("A required field is missing");
        } else {

            /* Suspect name/desc uniqueness check */
            MSHelp.getInstance(getApplicationContext());
            if(MSHelp.SuspectDAO.nameIsUnique(0, suspectNameDescInput.getText().toString())) {

                checkedGenderRB = (RadioButton) findViewById(suspectGenderRG.getCheckedRadioButtonId());
                suspectHeightInput = (NumberPicker) findViewById(R.id.suspectHeightInput);
                suspectWeightInput = (NumberPicker) findViewById(R.id.suspectWeightInput);
                suspectVehicleDescInput = (EditText) findViewById(R.id.suspectVehicleDescInput);
                suspectExtraInfoInput = (EditText) findViewById(R.id.suspectExtraInfoInput);

                new AlertDialog.Builder(this)
                        .setTitle("Suspect details review")
                        .setMessage(
                                "Name/Desc: \n" +
                                suspectNameDescInput.getText().toString() + "\n\n" +

                                "IC Code: \n" +
                                suspectIdentityCodeInput.getSelectedItem().toString() + "\n\n" +

                                "Gender: " + checkedGenderRB.getText() + "\n" +
                                "Est. height: " + getRealValue('h', suspectHeightInput.getValue()) + "cm\n" +
                                "Est. weight: " + getRealValue('w', suspectWeightInput.getValue()) + "kg\n" +
                                "Age range: " + suspectAgeRangeInput.getSelectedItem().toString() + "\n\n" +
                                "Vehicle desc: \n" +
                                MSTools.Text.textCheck(getApplicationContext(), suspectVehicleDescInput.getText().toString()) + "\n\n" +
                                "Additional info:" + "\n" +
                                MSTools.Text.textCheck(getApplicationContext(), suspectExtraInfoInput.getText().toString()))
                        .setNeutralButton("Go back", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            /* Initialize Singleton (if necessary) */
                                MSHelp.getInstance(getApplicationContext());
                                Suspect newSuspect = MSHelp.SuspectDAO.addSuspect(
                                        suspectNameDescInput.getText().toString(),
                                        suspectIdentityCodeInput.getSelectedItem().toString(),
                                        MSTools.Gender.getGenderChar(checkedGenderRB.getText().toString()),
                                        getRealValue('h', suspectHeightInput.getValue()),
                                        getRealValue('w', suspectWeightInput.getValue()),
                                        suspectAgeRangeInput.getSelectedItem().toString(),
                                        suspectVehicleDescInput.getText().toString(),
                                        suspectExtraInfoInput.getText().toString()
                                );


                                /* Add new suspect data in FTS Table.
                                * Operations, Insert/Update and Delete take longer than
                                * a operation on a standard table. FTS maintains an index that is udpated
                                * each time an operation is done. */
                                AddSuspectFTS asyncAddFTS = new AddSuspectFTS();
                                asyncAddFTS.execute(newSuspect);

                                Intent suspectIntent = new Intent(getApplicationContext(), SuspectActivity.class);

                            /* Suspect class implements Parceable (which is faster than Serializable) */
                                suspectIntent.putExtra(MSTools.Extra.SUSPECT_ACTIVITY_EXTRA_NEW, newSuspect);
                                startActivity(suspectIntent);

//                            showToast("Suspect added successfully");
                            }
                        }).show();



            } else {
                showToast("Name or description given is already taken! Please enter another one");

            }

        }


    }

    private class AddSuspectFTS extends AsyncTask<Suspect, Integer, Long> {

        /* Param 1: param for the doinBackground method
         * Param 2: used when showing the progress of the task (not used here)
         * Param 3: type of the result */
        @Override
        protected Long doInBackground(Suspect... params) {

            SQLiteDatabase db = MSHelp.getInstance(getApplicationContext()).getWritableDatabase();

            /* docid, is an auto-generated column that behaves like the primary key of the record/row
             * _id, is added as the CursorAdapter used in the SuspectListAndSearch activity
              * needs to take in parameter a Cursor with a column '_id'. */
            ContentValues newSuspectCtn = new ContentValues();
            newSuspectCtn.put(MSContr.SuspectEntry.COL_DOCID, params[0].get_id());
            newSuspectCtn.put(MSContr.SuspectEntry._ID, params[0].get_id());
            newSuspectCtn.put(MSContr.SuspectEntry.COL1_NAME_DESC, params[0].getName());
            newSuspectCtn.put(MSContr.SuspectEntry.COL2_IDENTITY_CODE, params[0].getIdentityCode());
            newSuspectCtn.put(MSContr.SuspectEntry.COL3_GENDER, String.valueOf(params[0].getGender()));
            newSuspectCtn.put(MSContr.SuspectEntry.COL4_HEIGHT, params[0].getHeight());
            newSuspectCtn.put(MSContr.SuspectEntry.COL5_WEIGHT, params[0].getWeight());
            newSuspectCtn.put(MSContr.SuspectEntry.COL6_AGE_RANGE, params[0].getAgeRange());
            newSuspectCtn.put(MSContr.SuspectEntry.COL7_VEHICLE_DESC, params[0].getVehicleDesc());
            newSuspectCtn.put(MSContr.SuspectEntry.COL8_EXTRA_INFO, params[0].getExtraInfo());
            newSuspectCtn.put(MSContr.SuspectEntry.COL9_CREATION_DATE, MSTools.Date.getDateFormat("").format(params[0].getCreationDate()));

            long result = db.insertOrThrow(MSContr.SuspectEntry.TABLE_NAME_FTS, null, newSuspectCtn);

            db.close();

            return result;
        }

        @Override
        protected void onPostExecute(Long result) {
//            super.onPostExecute(result);
            Log.d("FR_DEBUG", "addSuspectFTS, new Supect ID: " + result);
        }
    }

    private void showToast(String message) {
        Toast.makeText(AddSuspectActivity.this, message, Toast.LENGTH_SHORT).show();
    }

}
