package uk.ac.gre.fr7154a.madsleuth.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.text.ParseException;
import java.util.Date;

import uk.ac.gre.fr7154a.madsleuth.tools.MSTools;

/**
 * Created by ddzfred on 01/11/2015.
 */
public class Report implements Parcelable {

    private long _id;
    private Date reportTime;
    private String reportLocation;
    private String reportNotes;
    private long suspectId;
    private Date creationDate;


    /* ********************
     * PARCEABLE SPECIFIC *
     * ********************/

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(_id);
        dest.writeString(MSTools.Date.getDateFormat("").format(reportTime));
        dest.writeString(reportLocation);
        dest.writeString(reportNotes);
        dest.writeLong(suspectId);
        dest.writeString(MSTools.Date.getDateFormat("").format(creationDate));
    }

    private Report(Parcel in) throws ParseException {
        _id = in.readLong();
        reportTime = MSTools.Date.getDateFormat("").parse(in.readString());
        reportLocation = in.readString();
        reportNotes = in.readString();
        suspectId = in.readLong();
        creationDate = MSTools.Date.getDateFormat("").parse(in.readString());
    }

    public static final Parcelable.Creator<Report> CREATOR
            = new Creator<Report>() {
        @Override
        public Report createFromParcel(Parcel source) {
            Report rep = null;
            try {
                rep = new Report(source);
            } catch (ParseException pe) {
                Log.e("PARSING ERROR", "Instanciation from parcel failed: " + pe.getMessage());
            } finally {
                return rep;
            }
        }

        @Override
        public Report[] newArray(int size) {
            return new Report[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }



    /* **************************
     * STANDARD CLASS CODE CODE *
     ****************************/

    public Report() {

    }

    public Report(Date reportTime, String reportLocation, String reportNotes,
                  long suspectId, Date creationDate) {
        this.reportTime = reportTime;
        this.reportLocation = reportLocation;
        this.reportNotes = reportNotes;
        this.suspectId = suspectId;
        this.creationDate = creationDate;
    }

    /* Used when instanciating from DB data */
    public Report(long _id, String reportTime, String reportLocation,
                  String reportNotes, String creationDate, long suspectId) throws ParseException {
        this._id = _id;
        this.reportTime = MSTools.Date.getDateFormat("dddt").parse(reportTime);
        this.reportLocation = reportLocation;
        this.reportNotes = reportNotes;
        this.creationDate = MSTools.Date.getDateFormat("dddt").parse(creationDate);
        this.suspectId = suspectId;

    }

    @Override
    public boolean equals(Object o) {
        boolean b = false;
        if (o instanceof Report) {
            Report tmp = (Report) o;
            b = (this.get_id() == tmp.get_id());
        }
        return b;
    }


    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public Date getReportTime() {
        return reportTime;
    }

    public void setReportTime(Date reportTime) {
        this.reportTime = reportTime;
    }

    public String getReportLocation() {
        return reportLocation;
    }

    public void setReportLocation(String reportLocation) {
        this.reportLocation = reportLocation;
    }

    public String getReportNotes() {
        return reportNotes;
    }

    public void setReportNotes(String reportNotes) {
        this.reportNotes = reportNotes;
    }

    public long getSuspectId() {
        return suspectId;
    }

    public void setSuspectId(long suspectId) {
        this.suspectId = suspectId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
