package uk.ac.gre.fr7154a.madsleuth.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.text.ParseException;
import java.util.Date;

import uk.ac.gre.fr7154a.madsleuth.tools.MSTools;

/**
 * Created by ddzfred on 01/11/2015.
 */
public class Suspect implements Parcelable{

    private long _id;
    private String name;
    private String identityCode;
    private char gender;
    private int height;
    private int weight;
    private String ageRange;
    private String vehicleDesc;
    private String extraInfo;
    private Date creationDate;


    /* ********************
     * PARCEABLE SPECIFIC *
     * ********************/

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(_id);
        dest.writeString(name);
        dest.writeString(identityCode);
        dest.writeString("" + gender);
        dest.writeInt(height);
        dest.writeInt(weight);
        dest.writeString(ageRange);
        dest.writeString(vehicleDesc);
        dest.writeString(extraInfo);
        dest.writeString(MSTools.Date.getDateFormat("").format(creationDate));
    }

    private Suspect(Parcel in) throws ParseException {
        _id = in.readLong();
        name = in.readString();
        identityCode = in.readString();
        gender = in.readString().charAt(0);
        height = in.readInt();
        weight = in.readInt();
        ageRange = in.readString();
        vehicleDesc = in.readString();
        extraInfo = in.readString();
        creationDate = MSTools.Date.getDateFormat("").parse(in.readString());
    }

    public static final Parcelable.Creator<Suspect> CREATOR
            = new Creator<Suspect>() {
        @Override
        public Suspect createFromParcel(Parcel source) {
            Suspect su = null;
            try {
                su = new Suspect(source);
            } catch (ParseException pe) {
                Log.e("PARSING ERROR", "Instanciation from parcel failed: " + pe.getMessage());
            } finally {
                return su;
            }
        }

        @Override
        public Suspect[] newArray(int size) {
            return new Suspect[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    /* **************************
     * STANDARD CLASS CODE CODE *
     ****************************/

    /* Constructor #1 */
    public Suspect() {

    }

    /* Constructor #2 */
    public Suspect(String name,
                   String identityCode,
                   char gender,
                   int height,
                   int weight,
                   String ageRange,
                   String vehicleDesc,
                   String extraInfo,
                   Date creationDate) {
        this.name = name;
        this.identityCode = identityCode;
        this.gender = gender;
        this.height = height;
        this.weight = weight;
        this.ageRange = ageRange;
        this.vehicleDesc = vehicleDesc;
        this.extraInfo = extraInfo;
        this.creationDate = creationDate;
    }


    /* Used when instanciating from DB data
     * The cursor doesn't have a getChar() function, that's
     * why the gender is taken as a String
     * */
    public Suspect(long _id,
                   String name,
                   String identityCode,
                   String gender,
                   int height,
                   int weight,
                   String ageRange,
                   String vehicleDesc,
                   String extraInfo,
                   String creationDate) throws ParseException{
        this._id = _id;
        this.name = name;
        this.identityCode = identityCode;
        this.gender = gender.charAt(0);
        this.height = height;
        this.weight = weight;
        this.ageRange = ageRange;
        this.vehicleDesc = vehicleDesc;
        this.extraInfo = extraInfo;
        this.creationDate = MSTools.Date.getDateFormat("dddt").parse(creationDate);
    }


    @Override
    public boolean equals(Object o) {
        boolean b = false;
        if(o instanceof Suspect) {
            Suspect tmp = (Suspect) o;
            b = (this.get_id() == tmp.get_id());
        }
        return b;
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getAgeRange() {
        return ageRange;
    }

    public void setAgeRange(String ageRange) {
        this.ageRange = ageRange;
    }

    public String getVehicleDesc() {
        return vehicleDesc;
    }

    public void setVehicleDesc(String vehicleDesc) {
        this.vehicleDesc = vehicleDesc;
    }

    public String getExtraInfo() {
        return extraInfo;
    }

    public void setExtraInfo(String extraInfo) {
        this.extraInfo = extraInfo;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getIdentityCode() {
        return identityCode;
    }

    public void setIdentityCode(String identityCode) {
        this.identityCode = identityCode;
    }
}
