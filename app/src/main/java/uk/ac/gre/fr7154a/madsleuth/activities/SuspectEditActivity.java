package uk.ac.gre.fr7154a.madsleuth.activities;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import uk.ac.gre.fr7154a.madsleuth.R;
import uk.ac.gre.fr7154a.madsleuth.data.MSContr;
import uk.ac.gre.fr7154a.madsleuth.data.MSHelp;
import uk.ac.gre.fr7154a.madsleuth.interfaces.HeightAndWeightAble;
import uk.ac.gre.fr7154a.madsleuth.models.Suspect;
import uk.ac.gre.fr7154a.madsleuth.tools.MSTools;

public class SuspectEditActivity extends MSBaseActivity implements HeightAndWeightAble {

    private Suspect currentSuspect;

    private TextView seaTitle;

    private EditText seaSuspectNameDescInput;

    private Spinner seaSuspectIdentityCodeInput;
    private ArrayAdapter<CharSequence> seaIdentityCodeAdapter;

    private RadioGroup seaSuspectGenderRadioGroup;
    private RadioButton seaCheckedRadioBtn;

    private NumberPicker seaSuspectHeightInput;
    private int seaHeightValuesLength;
    private String[] seaHeightValues;

    private NumberPicker seaSuspectWeightInput;
    private int seaWeightValuesLength;
    private String[] seaWeightValues;

    private Spinner seaSuspectAgeRangeInput;
    private ArrayAdapter<CharSequence> seaAgeRangeAdapter;

    private EditText seaSuspectVehicleDescInput;

    private EditText seaSuspectExtraInfoInput;

    private Button seaCancelBtn;
    private Button seaSaveBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("FR_DEBUG", this.getClass().getSimpleName() + " - OnCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suspect_edit);

        if (getIntent().getParcelableExtra(MSTools.Extra.SUSPECT_EDIT_ACTIVITY_EXTRA) != null) {
            currentSuspect = getIntent().getParcelableExtra(MSTools.Extra.SUSPECT_EDIT_ACTIVITY_EXTRA);
        } else {
            Intent noDataIntent = new Intent();
            noDataIntent.putExtra(MSTools.Extra.CHILD_ACTIVITY_RESULT, "noData");
            setResult(RESULT_CANCELED);
        }

        layoutInit();

    }

    private void layoutInit() {

        seaTitle = (TextView) findViewById(R.id.seaTitle);
        seaTitle.append(Long.toString(currentSuspect.get_id()));

        seaSuspectNameDescInput = (EditText) findViewById(R.id.seaSuspectNameDescInput);
        seaSuspectNameDescInput.setText(currentSuspect.getName());

        identityCodeInit();

        genderInputInit();

        heightAndWeightInputsInit();

        ageRangeInputInit();

        seaSuspectVehicleDescInput = (EditText) findViewById(R.id.seaSuspectVehicleDescInput);
        seaSuspectVehicleDescInput.setText(currentSuspect.getVehicleDesc());

        seaSuspectExtraInfoInput = (EditText) findViewById(R.id.seaSuspectExtraInfoInput);
        seaSuspectExtraInfoInput.setText(currentSuspect.getExtraInfo());

        buttonsInit();

    }

    private void identityCodeInit() {
        seaSuspectIdentityCodeInput = (Spinner) findViewById(R.id.seaSuspectIdentityCodeInput);
        seaIdentityCodeAdapter = ArrayAdapter.createFromResource(
                this,
                R.array.identity_codes,
                android.R.layout.simple_spinner_item
        );
        seaIdentityCodeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        seaSuspectIdentityCodeInput.setAdapter(seaIdentityCodeAdapter);


        String[] identityCodes = getResources().getStringArray(R.array.identity_codes);

        int identityCodePosition = 0;
        while (!currentSuspect.getIdentityCode().equals(identityCodes[identityCodePosition])) {
            identityCodePosition++;
        }

        seaSuspectIdentityCodeInput.setSelection(identityCodePosition);
    }

    private void genderInputInit() {

        seaSuspectGenderRadioGroup = (RadioGroup) findViewById(R.id.seaSuspectGenderRadioGroup);

        switch (currentSuspect.getGender()) {

            case 'm':
                seaCheckedRadioBtn = (RadioButton) findViewById(R.id.seaRadioBtnMale);
                break;

            case 'f':
                seaCheckedRadioBtn = (RadioButton) findViewById(R.id.seaRadioBtnFemale);
                break;

            case 'u':
                seaCheckedRadioBtn = (RadioButton) findViewById(R.id.seaRadioBtnUnknown);
                break;

            default:
                seaCheckedRadioBtn = (RadioButton) findViewById(R.id.seaRadioBtnUnknown);
                break;

        }
        seaCheckedRadioBtn.setChecked(true);

    }

    @Override
    public void heightAndWeightInputsInit() {

        /* Suspect Height Input Init */
        seaSuspectHeightInput = (NumberPicker) findViewById(R.id.seaSuspectHeightInput);
        seaHeightValuesLength = (HEIGHT_MAX - HEIGHT_MIN) / HEIGHT_INTERVAL + 1;
        seaHeightValues = new String[seaHeightValuesLength];
        for (int i = 0; i < seaHeightValuesLength; i++) {
            seaHeightValues[i] = Integer.toString(HEIGHT_MIN + HEIGHT_INTERVAL * i);
        }
        seaSuspectHeightInput.setMinValue(0);
        seaSuspectHeightInput.setMaxValue(seaHeightValuesLength - 1);
        seaSuspectHeightInput.setWrapSelectorWheel(false);
        seaSuspectHeightInput.setDisplayedValues(seaHeightValues);
        seaSuspectHeightInput.setValue(getPickerValue('h', currentSuspect.getHeight()));


        /* Suspect Weight Input Init */
        seaSuspectWeightInput = (NumberPicker) findViewById(R.id.seaSuspectWeightInput);
        seaWeightValuesLength = (WEIGHT_MAX - WEIGHT_MIN) / WEIGHT_INTERVAL + 1;
        seaWeightValues = new String[seaWeightValuesLength];
        for (int i = 0; i < seaWeightValuesLength; i++) {
            seaWeightValues[i] = Integer.toString(WEIGHT_MIN + WEIGHT_INTERVAL * i);
        }
        seaSuspectWeightInput.setMinValue(0);
        seaSuspectWeightInput.setMaxValue(seaWeightValuesLength - 1);
        seaSuspectWeightInput.setWrapSelectorWheel(false);
        seaSuspectWeightInput.setDisplayedValues(seaWeightValues);
        seaSuspectWeightInput.setValue(getPickerValue('w', currentSuspect.getWeight()));
    }

    @Override
    public int getRealValue(char type, int pickerValue) {

        switch (type) {

            /* HEIGHT */
            case 'h':
                return HEIGHT_MIN + pickerValue * HEIGHT_INTERVAL;

            /* WEIGHT */
            case 'w':
                return WEIGHT_MIN + pickerValue * WEIGHT_INTERVAL;

            default:
                Log.d("FR_DEBUG", "Wrong type");
                return 0;
        }
    }

    private int getPickerValue(char type, int realValue) {

        switch (type) {

            /* HEIGHT */
            case 'h':
                return (realValue - HEIGHT_MIN) / HEIGHT_INTERVAL;

            case 'w':
                return (realValue - WEIGHT_MIN) / WEIGHT_INTERVAL;

            default:
                return 0;

        }

    }

    private void ageRangeInputInit() {

        seaSuspectAgeRangeInput = (Spinner) findViewById(R.id.seaSuspectAgeRangeInput);
        seaAgeRangeAdapter = ArrayAdapter.createFromResource(
                this,
                R.array.age_ranges,
                android.R.layout.simple_spinner_item
        );
        seaAgeRangeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        seaSuspectAgeRangeInput.setAdapter(seaAgeRangeAdapter);

        String[] ageRanges = getResources().getStringArray(R.array.age_ranges);

        int ageRangePosition = 0;
        while (!currentSuspect.getAgeRange().equals(ageRanges[ageRangePosition])) {
            ageRangePosition++;
        }

        seaSuspectAgeRangeInput.setSelection(ageRangePosition);

    }

    private void buttonsInit() {
        seaCancelBtn = (Button) findViewById(R.id.seaCancelBtn);
        seaSaveBtn = (Button) findViewById(R.id.seaSaveBtn);

        seaCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelBtnEvent();
            }
        });

        seaSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveBtnEvent();
            }
        });

    }

    private void cancelBtnEvent() {
        Intent cancelIntent = new Intent();
        cancelIntent.putExtra(MSTools.Extra.CHILD_ACTIVITY_RESULT, "editCancelled");
        setResult(RESULT_CANCELED, cancelIntent);
        finish();
    }

    private void saveBtnEvent() {

        if (seaSuspectNameDescInput.getText().toString().isEmpty()) {

            showToast("Please put a suspect name or description!");

        } else {

            MSHelp.getInstance(getApplicationContext());
            if (MSHelp.SuspectDAO.nameIsUnique(currentSuspect.get_id(), seaSuspectNameDescInput.getText().toString())) {

                seaCheckedRadioBtn = (RadioButton) findViewById(seaSuspectGenderRadioGroup.getCheckedRadioButtonId());

                new AlertDialog.Builder(SuspectEditActivity.this)
                        .setTitle("Updated suspect details review")
                        .setMessage(
                                "Name/Desc: \n" +
                                        seaSuspectNameDescInput.getText().toString() + "\n\n" +

                                        "IC Code: \n" +
                                        seaSuspectIdentityCodeInput.getSelectedItem().toString() + "\n\n" +

                                        "Gender: " + seaCheckedRadioBtn.getText() + "\n" +
                                        "Est. height: " + getRealValue('h', seaSuspectHeightInput.getValue()) + "cm\n" +
                                        "Est. weight: " + getRealValue('w', seaSuspectWeightInput.getValue()) + "kg\n" +
                                        "Age range: " + seaSuspectAgeRangeInput.getSelectedItem().toString() + "\n\n" +

                                        "Vehicle desc: \n" +
                                        MSTools.Text.textCheck(getApplicationContext(), seaSuspectVehicleDescInput.getText().toString()) + "\n\n" +

                                        "Additional info: \n" +
                                        MSTools.Text.textCheck(getApplicationContext(), seaSuspectExtraInfoInput.getText().toString())
                        )
                        .setNeutralButton("Go back", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                /* Do nothing */
                            }
                        })
                        .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                /* Save new values inside currentSuspect */
                                currentSuspect.setName(seaSuspectNameDescInput.getText().toString());
                                currentSuspect.setIdentityCode(seaSuspectIdentityCodeInput.getSelectedItem().toString());
                                currentSuspect.setGender(MSTools.Gender.getGenderChar(seaCheckedRadioBtn.getText().toString()));
                                currentSuspect.setHeight(getRealValue('h', seaSuspectHeightInput.getValue()));
                                currentSuspect.setWeight(getRealValue('w', seaSuspectWeightInput.getValue()));
                                currentSuspect.setAgeRange(seaSuspectAgeRangeInput.getSelectedItem().toString());
                                currentSuspect.setVehicleDesc(seaSuspectVehicleDescInput.getText().toString());
                                currentSuspect.setExtraInfo(seaSuspectExtraInfoInput.getText().toString());

                                MSHelp.getInstance(getApplicationContext());
                                int updateStatus = MSHelp.SuspectDAO.updateSuspect(currentSuspect);

                                Intent saveIntent = new Intent();
                                if (updateStatus > 0) {

                                    UpdateSuspectFTS asyncUpdateFTS = new UpdateSuspectFTS();
                                    asyncUpdateFTS.execute(currentSuspect);

                                    saveIntent.putExtra(MSTools.Extra.CHILD_ACTIVITY_RESULT, currentSuspect);
                                    setResult(RESULT_OK, saveIntent);

                                } else {

                                    saveIntent.putExtra(MSTools.Extra.CHILD_ACTIVITY_RESULT, "updateFailed");
                                    setResult(RESULT_CANCELED, saveIntent);

                                }
                                finish();

                            }
                        })
                        .show();

            } else {
                showToast("This name/description is already taken, please choose something else!");
            }

        }
    }

    private class UpdateSuspectFTS extends AsyncTask<Suspect, Integer, Integer> {

            /* TAKES IN PARAMETER THE UPDATED SUSPECT */

        @Override
        protected Integer doInBackground(Suspect... params) {

            SQLiteDatabase db = MSHelp.getInstance(getApplicationContext()).getWritableDatabase();

            ContentValues updatedValues = new ContentValues();
            updatedValues.put(MSContr.SuspectEntry.COL1_NAME_DESC, params[0].getName());
            updatedValues.put(MSContr.SuspectEntry.COL2_IDENTITY_CODE, params[0].getIdentityCode());
            updatedValues.put(MSContr.SuspectEntry.COL3_GENDER, String.valueOf(params[0].getGender()));
            updatedValues.put(MSContr.SuspectEntry.COL4_HEIGHT, params[0].getHeight());
            updatedValues.put(MSContr.SuspectEntry.COL5_WEIGHT, params[0].getWeight());
            updatedValues.put(MSContr.SuspectEntry.COL6_AGE_RANGE, params[0].getAgeRange());
            updatedValues.put(MSContr.SuspectEntry.COL7_VEHICLE_DESC, params[0].getVehicleDesc());
            updatedValues.put(MSContr.SuspectEntry.COL8_EXTRA_INFO, params[0].getExtraInfo());

            int updateStatus = db.update(
                    MSContr.SuspectEntry.TABLE_NAME_FTS,
                    updatedValues,
                    MSContr.SuspectEntry.COL_DOCID + " = ?",
                    new String[]{"" + params[0].get_id()});

            db.close();
            return updateStatus;

        }

        @Override
        protected void onPostExecute(Integer updateStatus) {
            super.onPostExecute(updateStatus);
            Log.d("FR_DEBUG", "updateSuspectFTS status: " + updateStatus);
        }
    }


    private void showToast(String message) {
        Toast.makeText(SuspectEditActivity.this, message, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onBackPressed() {
        cancelBtnEvent();
    }

    @Override
    protected void onResume() {
        Log.d("FR_DEBUG", this.getClass().getSimpleName() + " - OnResume");
        super.onResume();
    }

    @Override
    protected void onStart() {
        Log.d("FR_DEBUG", this.getClass().getSimpleName() + " - OnStart");
        super.onStart();
    }

    @Override
    protected void onStop() {
        Log.d("FR_DEBUG", this.getClass().getSimpleName() + " - OnStop");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d("FR_DEBUG", this.getClass().getSimpleName() + " - OnDestroy");
        super.onDestroy();
    }
}
