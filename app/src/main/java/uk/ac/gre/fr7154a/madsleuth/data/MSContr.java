package uk.ac.gre.fr7154a.madsleuth.data;

import android.provider.BaseColumns;

/**
 * Created by ddzfred on 31/10/2015.
 */
public final class MSContr {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "madsleuth.db";

    /* These strings will be reused a few times. Also, using constants
     * will prevent making mistakes */
    private static final String SQL_CREATE_TABLE = "CREATE TABLE ";
    private static final String SQL_PK_AUTOINCREMENT = " PRIMARY KEY AUTOINCREMENT";
    private static final String SQL_FK = "FOREIGN KEY";
    private static final String SQL_REF = " REFERENCES ";
    private static final String SQL_TEXT_TYPE = " TEXT";
    private static final String SQL_REAL_TYPE = " REAL";
    private static final String SQL_INT_TYPE = " INTEGER";
    private static final String SQL_UNIQUE = " UNIQUE";
    private static final String SQL_NOT_NULL = " NOT NULL";
    private static final String SQL_DROP_TABLE = "DROP TABLE IF EXISTS ";
    private static final String SQL_DROP_INDEX = "DROP INDEX IF EXISTS ";
    private static final String COMMA_SEP = ", ";

    /* Allows to enable foreign key constraints */
    public static final String ENABLE_FOREIGN_KEYS = "PRAGMA foreign_keys=ON;";

    /*     If return 0, then support is OFF and have to be turned ON
         If return 1, then support is ON, nothing to do!
         If return NOTHING, then either SQLite version is older than 3.6.19
         OR it was compiled with SQLITE_OMIT_FOREIGN_KEY
         OR SQLITE_OMIT_TRIGGER is defined*/
    public static final String IS_FK_SUPPORTED = "PRAGMA foreign_keys";

    private MSContr() {}


    /* SUSPECT TABLE */
    public static abstract class SuspectEntry implements BaseColumns {

        public static final String TABLE_NAME = "suspect";
        public static final String TABLE_NAME_FTS = "suspect_fts";
        public static final String INDEX_NAME = "namedesc_unique_index";

        /* Columns name (_ID is COL 0 and provided by BaseColumns) */
        public static final String COL1_NAME_DESC = "namedesc";
        public static final String COL2_IDENTITY_CODE = "iccode";
        public static final String COL3_GENDER = "gender";
        public static final String COL4_HEIGHT = "height";
        public static final String COL5_WEIGHT = "weight";
        public static final String COL6_AGE_RANGE = "agerange";
        public static final String COL7_VEHICLE_DESC = "vehicledesc";
        public static final String COL8_EXTRA_INFO = "extrainfo";
        public static final String COL9_CREATION_DATE = "creationdate";

        /* Used when doing operation on the FTS Table.
         * It is an auto-generated column */
        public static final String COL_DOCID = "docid";


        /* 1. Array of columns: used when reading the DB */

        /* 1.1. Array #1 = All the columns */
        public static final String[] COLUMNS = {
                _ID,
                COL1_NAME_DESC,
                COL2_IDENTITY_CODE,
                COL3_GENDER,
                COL4_HEIGHT,
                COL5_WEIGHT,
                COL6_AGE_RANGE,
                COL7_VEHICLE_DESC,
                COL8_EXTRA_INFO,
                COL9_CREATION_DATE
        };

         /* 1.2. Array #2 = All but the _ID */
         public static final String[] COLUMNS_NO_ID = {
                 COL1_NAME_DESC,
                 COL2_IDENTITY_CODE,
                 COL3_GENDER,
                 COL4_HEIGHT,
                 COL5_WEIGHT,
                 COL6_AGE_RANGE,
                 COL7_VEHICLE_DESC,
                 COL8_EXTRA_INFO,
                 COL9_CREATION_DATE
         };

        /* 1.3. Array #3 = _ID only */
        public static final String[] COLUMNS_ID_ONLY = { _ID};

        public static final String[] FTS_COLUMNS = {
                _ID,
                COL1_NAME_DESC,
                COL2_IDENTITY_CODE,
                COL3_GENDER,
                COL4_HEIGHT,
                COL5_WEIGHT,
                COL6_AGE_RANGE,
                COL7_VEHICLE_DESC,
                COL8_EXTRA_INFO,
                COL9_CREATION_DATE
        };

        public static final String[] FTS_COLUMNS_ID_AND_NAME = {
                _ID,
                COL1_NAME_DESC
        };

        public static final String CREATE_TABLE =
                SQL_CREATE_TABLE + TABLE_NAME + " (" +
                    _ID + SQL_INT_TYPE + SQL_PK_AUTOINCREMENT + COMMA_SEP +
                    COL1_NAME_DESC + SQL_TEXT_TYPE + COMMA_SEP +
                    COL2_IDENTITY_CODE + SQL_TEXT_TYPE + COMMA_SEP +
                    COL3_GENDER + SQL_TEXT_TYPE + COMMA_SEP +
                    COL4_HEIGHT + SQL_INT_TYPE + COMMA_SEP +
                    COL5_WEIGHT + SQL_INT_TYPE + COMMA_SEP +
                    COL6_AGE_RANGE + SQL_TEXT_TYPE + COMMA_SEP +
                    COL7_VEHICLE_DESC + SQL_TEXT_TYPE + COMMA_SEP +
                    COL8_EXTRA_INFO + SQL_TEXT_TYPE + COMMA_SEP +
                    COL9_CREATION_DATE + SQL_TEXT_TYPE +
                ")";
        public static final String CREATE_TABLE_FTS =
                "CREATE VIRTUAL TABLE " + TABLE_NAME_FTS + " USING fts3(" +
                        _ID + COMMA_SEP +
                        COL1_NAME_DESC + COMMA_SEP +
                        COL2_IDENTITY_CODE + COMMA_SEP +
                        COL3_GENDER + COMMA_SEP +
                        COL4_HEIGHT + COMMA_SEP +
                        COL5_WEIGHT + COMMA_SEP +
                        COL6_AGE_RANGE + COMMA_SEP +
                        COL7_VEHICLE_DESC + COMMA_SEP +
                        COL8_EXTRA_INFO + COMMA_SEP +
                        COL9_CREATION_DATE +
                        ")";

        public static final String DROP_TABLE = SQL_DROP_TABLE + TABLE_NAME;
        public static final String DROP_TABLE_FTS = SQL_DROP_TABLE + TABLE_NAME_FTS;
        public static final String CREATE_INDEX =
                "CREATE UNIQUE INDEX IF NOT EXISTS " + INDEX_NAME +
                " ON " + TABLE_NAME + " (" + COL1_NAME_DESC + ")";
        public static final String DROP_INDEX = SQL_DROP_INDEX + INDEX_NAME;

    }


    /* REPORT TABLE */
    public static abstract class ReportEntry implements BaseColumns {

        public static final String TABLE_NAME = "report";
        public static final String INDEX_NAME = "suspect_id_index";

        /* Columns name (_ID is COL 0 and provided by BaseColumns) */
        public static final String COL1_REPORT_TIME = "reporttime";
        public static final String COL2_REPORT_LOCATION = "reportlocation";
        public static final String COL3_REPORT_NOTES = "reportnotes";
        public static final String COL4_CREATION_DATE = "creationdate";
        public static final String COL5_SUSPECT_ID_FK = "suspect_id";


        /* 1. Array of columns: used when reading the DB */
        /* 1.1. Array #1 = All the columns */
        public static final String [] COLUMNS = {
                _ID,
                COL1_REPORT_TIME,
                COL2_REPORT_LOCATION,
                COL3_REPORT_NOTES,
                COL4_CREATION_DATE,
                COL5_SUSPECT_ID_FK
        };
        /* 1.2. Array #2 = All but the _ID */
        public static final String[] COLUMNS_NO_ID = {
                COL1_REPORT_TIME,
                COL2_REPORT_LOCATION,
                COL3_REPORT_NOTES,
                COL4_CREATION_DATE,
                COL5_SUSPECT_ID_FK
        };
        /* 1.3. Array #3 = _ID only */
        public static final String[] COLUMNS_ID_ONLY = { _ID};


        public static final String CREATE_TABLE =
                SQL_CREATE_TABLE + TABLE_NAME + " (" +
                    _ID + SQL_INT_TYPE + SQL_PK_AUTOINCREMENT + COMMA_SEP +
                    COL1_REPORT_TIME + SQL_TEXT_TYPE + COMMA_SEP +
                    COL2_REPORT_LOCATION + SQL_TEXT_TYPE + COMMA_SEP +
                    COL3_REPORT_NOTES + SQL_TEXT_TYPE + COMMA_SEP +
                    COL4_CREATION_DATE + SQL_TEXT_TYPE + COMMA_SEP +
                    COL5_SUSPECT_ID_FK + SQL_INT_TYPE + SQL_NOT_NULL + COMMA_SEP +
                    SQL_FK + "(" + COL5_SUSPECT_ID_FK + ")" +
                    SQL_REF + SuspectEntry.TABLE_NAME + "(" + SuspectEntry._ID + ")" +
                ")";

        public static final String DROP_TABLE = SQL_DROP_TABLE + TABLE_NAME;
        public static final String CREATE_INDEX =
                "CREATE INDEX " + INDEX_NAME +
                " ON " + TABLE_NAME + " (" + COL5_SUSPECT_ID_FK + ")";
        public static final String DROP_INDEX = SQL_DROP_INDEX + INDEX_NAME;
    }
}
